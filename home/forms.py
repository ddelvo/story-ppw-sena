from django import forms

YEAR_CHOICES = (
    ('2020', '2020'),
    ('2019', '2019'),
)
CLASS_CHOICES = (
    ('A', 'A'),
    ('B', 'B'),
    ('C', 'C'),
    ('D', 'D'),
    ('E', 'E'),
    ('F', 'F'),
    ('G', 'G'),
    ('H', 'H'),
    ('I', 'I'),
    ('J', 'J'),
    ('K', 'K'),
    ('L', 'L'),
    ('M', 'M'),
)
SKS_CHOICES = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
    ('6', '6'),
)

class KuliahForm(forms.Form):
    matkul = forms.CharField(label="Mata Kuliah", max_length=100)
    dosen = forms.CharField(label="Dosen", max_length=100)
    jumlahSks = forms.ChoiceField(label="Jumlah SKS", choices=SKS_CHOICES)
    deskripsi = forms.CharField(label="Deskripsi Kuliah",max_length=200, widget=forms.Textarea)
    tahun = forms.ChoiceField(label="Tahun", choices=YEAR_CHOICES)
    kelas = forms.ChoiceField(label="Kelas", choices=CLASS_CHOICES)

