from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('interests', views.interests, name="interests"),
    path('matakuliah', views.matakuliah, name="matakuliah"),
    path('listmatkul', views.listmatkul, name="listmatkul"),
    path('hapusMatkul/<str:pk>', views.hapusMatkul, name="hapusMatkul"),
    path('namamatkul', views.namaMatkul, name="namamatkul")
]