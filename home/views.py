from django.shortcuts import render, redirect
from .forms import KuliahForm
from .models import Kuliah

# Create your views here.
def index(request):
    return render(request, 'home/index.html')

def interests(request):
    return render(request, 'home/interests.html')

## Taken form django documentation
def matakuliah(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = KuliahForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            matkul = form.cleaned_data['matkul']
            dosen = form.cleaned_data['dosen']
            jumlahSks = form.cleaned_data['jumlahSks']
            deskripsi = form.cleaned_data['deskripsi']
            tahun = form.cleaned_data['tahun']
            kelas = form.cleaned_data['kelas']
            p = Kuliah(matkul = matkul, dosen=dosen, jumlahSks = jumlahSks, deskripsi =deskripsi, tahun=tahun,
                       kelas=kelas)
            p.save()
            return render(request, 'home/terimakasih.html')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = KuliahForm()
    return render(request, 'home/matakuliah.html', {'form': form})

def listmatkul(request):
    all = Kuliah.objects.all()
    return render(request, 'home/listmatkul.html', {'all':all})

def hapusMatkul(request, pk):
    kuliah = Kuliah.objects.get(id=pk)
    if request.method == "POST":
        kuliah.delete()
        return redirect("home:listmatkul")
    context= {'item':kuliah}
    return render(request, 'home/delete.html', context)

def namaMatkul(request):
    semua = Kuliah.objects.all()
    return render(request, 'home/namamatkul.html', {'all':semua})