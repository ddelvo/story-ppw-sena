from django.db import models

# Create your models here.


YEAR_CHOICES = (
    ('2020', '2020'),
    ('2019', '2019'),
)

CLASS_CHOICES = (
    ('A', 'A'),
    ('B', 'B'),
    ('C', 'C'),
    ('D', 'D'),
    ('E', 'E'),
    ('F', 'F'),
    ('G', 'G'),
    ('H', 'H'),
    ('I', 'I'),
    ('J', 'J'),
    ('K', 'K'),
    ('L', 'L'),
    ('M', 'M'),
)

SKS_CHOICES = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
    ('6', '6'),
)

class Kuliah(models.Model): #Representasi database
    matkul = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    jumlahSks = models.CharField(max_length=6, choices=SKS_CHOICES, default='1')
    deskripsi = models.TextField(max_length=200)
    tahun = models.CharField(max_length=6, choices=YEAR_CHOICES, default='2020')
    kelas = models.CharField(max_length=6, choices=CLASS_CHOICES, default='A')

