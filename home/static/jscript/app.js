const navSlide = () => {
    const burger = document.querySelector(".burger");
    const nav = document.querySelector(".nav_links");
    const nav1 = document.querySelector(".nav1");
    const nav2 = document.querySelector(".nav2");
    const nav3 = document.querySelector(".nav3");
    const nav4 = document.querySelector(".nav4");
    const nav5 = document.querySelector(".nav5");
    const nav6 = document.querySelector(".nav6");

    burger.addEventListener('click', () => {
        nav.classList.toggle("nav-active");
    });

    nav1.addEventListener("click", () => {
        nav.classList.toggle("nav-active");
    });

    nav2.addEventListener("click", () => {
        nav.classList.toggle("nav-active");
    });

    nav3.addEventListener("click", () => {
        nav.classList.toggle("nav-active");
    });

    nav4.addEventListener("click", () => {
        nav.classList.toggle("nav-active");
    });

    nav5.addEventListener("click", () => {
        nav.classList.toggle("nav-active");
    });

     nav6.addEventListener("click", () => {
        nav.classList.toggle("nav-active");
    });

}

navSlide();